��    �      l  �   �      �  
   �     �     �     �     �     �       
          k     `   �  g   �     Q     f     v  �   ~     5     A  \   V  
   �  Z   �          8     F     S     Z     p     �     �     �     �  ,   �  #   &     J     e     �  7   �  0   �  0   �  Z   (  :   �  5   �  :   �  :   /  0   j     �  3   �     �     �          .     >      K     l  
   |  -   �  .   �     �  K   �     I     N     c     x     �     �  	   �     �  %   �     �     �          9     T     `     l  	   y  {  �     �       
        &  @   4  >   u     �     �     �     �     �     �                    +     D     Y  	   w     �     �     �     �  )   �     �          !     <     Q  K   d     �     �     �     �  	   �     �            I   !  t   k     �  �   �  �   �     �  T   �     �     	  b     +   �     �     �     �     �  	   �     �           /     C     W  %   k  \  �  
   �     �     
      $      -      5      I      ^   
   g   o   r   p   �   f   S!     �!     �!  	   �!  �   �!     �"     �"  m   �"     _#  W   o#  !   �#     �#     �#     	$     $     %$     ?$     ^$  '   }$  $   �$  7   �$  *   %     -%      E%     f%  I   u%  C   �%  G   &  w   K&  R   �&  K   '  T   b'  V   �'  P   (     _(  7   v(     �(  !   �(     �(     )     )  %   1)     W)     k)  0   �)  .   �)     �)  ^   �)     \*     c*     z*     �*     �*     �*     �*     �*  -   �*      +     9+  #   T+  "   x+     �+     �+     �+     �+  �  �+     �-     �-     �-     �-  S   �-  P   .     f.     u.     �.     �.     �.     �.     �.     �.     �.     �.     /     /  	   =/     G/     W/     q/      �/  6   �/     �/  #   �/  *   0     G0     f0  ]   �0     �0     �0     1     $1  
   *1     51     N1     V1  I   n1  y   �1  *   22    ]2  �   n3  /   4  T   E4     �4     �4  y   �4      ;5     \5  	   s5     }5     �5     �5     �5  '   �5     �5     6     /6  ,   I6         8          q   �   3       P   T       �   /   0   +       ;   _          6   n   7       v   �   .      }   �   j       E       l   F   &   ]       p      u          Q      e   z           x       C          �   k   B           $       c   N      "                  U      *   Y   %               X           s   Z   W   \            G   o                   O   5           �   ^   :          D       ~   `           '   !   (   S       g   I   �   @   d               �       �   
       >      w       K   ,   r   {           9       ?   J              V          y   A   M              [   #   f       t   2   L   m      H              -          a   h          <   	   )   |   �   1   =   4   �   R             i   b         Address:   Birth-Dates:   Joined Date:   Level:   Name:   Others Info:   Phone:   Surname: &View A database has been imported successfully:

{0}

You must re-start the DrumsT application now.

Good Work ! A new drumsT rootdir has been created on:

{0}

Restart the DrumsT application now.

Good Work ! A new drumsT rootdir has been created:

{0}

You must re-start the DrumsT application now.

Good Work ! About DrumsT program Add New Student Address Are you sure to delete selected student?

Note that this process remove all stored
information and lessons register.

After this action all the deleted data will
not be recoverable ! Attendances Attendances Register Before editing cells in other rows, you need to 
render the changes with the 'Apply' button. Birth Date Can not find the configuration file!
Please, remove and reinstall the drumsT application· Change data to student profile Chart Reading Coordination Create Create a new database Create a new database: Create a new student profile Display lessons register DrumsT - Add new school year DrumsT - New school database.. DrumsT - School Management For Drum Teachers DrumsT - create new school database DrumsT: Already exist : %s DrumsT: Change Not Allowed DrumsT: Error DrumsT: Failed to add lesson in Lesson table

ERROR: %s DrumsT: Failed to add new school year

ERROR: %s DrumsT: Failed to create new database

ERROR: %s DrumsT: Failed to delete student and its lesson in Class table and Lesson table

ERROR: %s DrumsT: Failed to insert student in Class table

ERROR: %s DrumsT: Failed to make database root dir

OSError: %s DrumsT: Failed to update lesson in Lesson table

ERROR: %s DrumsT: Failed to update student in Class table

ERROR: %s DrumsT: Failed to write drumsT.conf:

ERROR: {0} DrumsT: Fatal Error DrumsT: New database has been successfully created! DrumsT: Please confirm DrumsT: Successfully stored! DrumsT: Unable to save DrumsT: Warning DrumsT: info DrumsT: open a school database.. DrumsT: warning Edit Topic Edit text or values into the cells and append Edit the '{0}' topic in the {1}th lesson       Elements of Style Rhythm Empty database: There isn't any list to load. You must add new students now Exit Exit and do nothing: Exit the application Hands/Foots Setting Hands/foots Setting ID class ID lesson Import Import a 'drumsT_DB' existing folder: Incomplete profile assignment! Independance/Coordination Insert lesson in the database ? It was added a new profile Joined Date Lesson Date Level-Course Minus One Music drums school management

DrumsT is a school manager application open-source and
cross-platform, focused to individual lessons and designed
for drums teachers. It can handle independently multiple
school locations with progressive data storing of several
school years and students lessons who learns
the art of drumming.

DrumsT is free software that respects your freedom!
 Name Name Database Imported No absence Note/Reminder On 'Previus lessons' there are unsaved data...
continue closing? On 'todays lesson' there are unsaved data...
continue closing? Online help Open a school database Open lessons register Other-1 Other-2 Other-3 Phone Please confirm Previous lessons Previous lessons with %s Reminder/Block-notes Restore to the previous state Rudiments School Year School Year selection School year: Set a new school year Set a new school year to opened database: Set new school year Show or hide status bar view Show or hide tool bar view Show/Hide Status Bar Show/Hide Tool Bar Some window is still open!
..Save your data before closing the application. Start New Lesson Student is absent Studies Topic Styles Success ! Successfully stored! Surname Teacher is absent The configuration file is wrong! Please, reinstall the drumsT application The root directory for saving databases no longer
exists! Do you want to import an existing one or
create a new one? This column can not be edit. This is the first time you start DrumsT.

DrumsT uses only one path to save/open databases.

- To create a new database now, press the create button.
- If a database already exists press the import button.
- Press the exit button to exit simply This name already exists:

NAME/SURNAME:  {0} {1}
PHONE:  {2}
ADDRESS:  {3}
BIRTHDATE:  {4}
JOINED DATE:  {5}
LEVEL:  {6}

Want you to save anyway? This school year already exist. To use DrumsT, you need to open a database or create a new one if it does not exist  Todays lesson Todays lesson with %s Type school name or a location identifier. This will create a new database with the named school:  Update and commit new entries into database Update new profile Votes Votes and Reminders Votes/Report Welcome ! Where do you want to save ? Where is the 'DrumsT_DB' folder? data change student data delete student lesson date setting {0} {1} with ID class {2} was deleted Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-02-12 18:49+0100
PO-Revision-Date: 2019-02-12 18:53+0100
Last-Translator: Gianluca (jeanslack) Pernigotto <jeanlucperni@gmail.com>
Language-Team: 
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
 Indirizzo: Data di Nascita: Data Inizio o Iscrizione: Livello:   Nome: Altre Informazioni: Recapito Telefonico: Cognome: Visualizza Un database è stato appena importato con successo:

{0}

Ora dovresti riavviare l'applicazione.

Buon Lavoro ! Una nuova cartella radice è stata appena creata in:

{0}

Ora dovresti riavviare l'applicazione.

Buon Lavoro ! E' stata creata una nuova cartella per i database:

{0}

Ora dovresti riavviare DrumsT.

Buon Lavoro ! Circa DrumsT Inserisci Nuovo Studente Indirizzo Si è sicuri di rimuovere lo studente selezionato?

Si consideri che questo processo rimuoverà tutte
le informazioni archiviate compreso i dati delle lezioni.

Dopo questa azione non sarà più possibile il ripristino
dei dati ! Presenze Registro delle Presenze Prima di modificare le celle di altre righe, dovresti
applicare i cambiamenti recenti con il tasto 'Applica'. Data di Nascita Impossibile trovare il file di configurazione!
Per favore, rimuovi e reinstalla DrumsT. Cambia i dati al profilo studente Lettura Partiture Coordinazione Crea Crea un nuovo database Creare un nuovo database: Crea un nuovo profilo studente Visualizza il registro lezioni DrumsT - Aggiungi nuovo anno scolastico DrumsT - Nuovo database scolastico.. DrumsT - Gestione scolastica per insegnanti di batteria DrumsT - crea un nuovo database scolastico DrumsT: Esiste già: %s DrumsT - Modifica non consentita DrumsT: Errore DrumsT: Registrazione dati lezione fallita su tavola lezione!

ERRORE: %s DrumsT: Registrazione Fallita! su nuovo anno scolastico

ERRORE: %s DrumsT: Registrazione Fallita! nel creare un nuovo database

ERRORE: %s DrumsT: Registrazione Fallita! su cancella dati studente e relative lezioni 
(Class table and Lesson table)

ERRORE: %s DrumsT: Registrazione Fallita nell'inserimento studente su Class table

ERRORE: %s DrumsT: Impossibile creare la directory principale del database

ERRORE: %s DrumsT: Registrazione Fallita nell'aggiornamento lezione 
(Lesson table)

ERRORE: %s DrumsT: Registrazione Fallita nell'aggiornare dati studente.
(Class table)

ERRORE: %s DrumsT: Impossibile scrivere sul file di configurazione
drumsT.conf

ERRORE: {0} DrumsT: Errore fatale! DrumsT: Un nuovo database è stato creato con successo! DrumsT - Prego confermare DrumsT - Archiviato con successo! DrumsT: Impossibile salvare DrumsT: Avvertimento DrumsT - informazione DrumsT: Apri un database scolastico.. DrumsT - Avvertenza Modifica Argomentazioni Modifica testo o valori nelle celle e aggiungili Modifica l'argomento '{0}' nella {1}° lezione Stili e linguaggi Ritmici Database vuoto: Nessuna lista caricata. Ora, dovresti aggiungere nuovi profili degli studenti. Uscita Esci e non fare niente Esci dall'applicazione Impostazioni Mani/Piedi Impostazioni Mani/Piedi ID della classe ID della lezione Importa Importare una cartella 'drumsT_DB' esistente: Assegnazione profilo incompleta! Indipendenza/Coordinazione Archiviare la lezione sul database? E' stato aggiunto un nuovo profilo Data di Inizio Data Lezione Livello del Corso Basi Musicali Gestione scolastica di batteria

DrumsT è un gestionale scolastico a sorgente aperto
multi-piattaforma, focalizzato su lezioni individuali e
progettato per insegnanti di batteria. Può gestire più
sedi scolastiche in modo indipendente con memorizzazione
progressiva di diversi anni scolastici e un numero illimitato
di studenti i quali imparano l'arte del drumming.

DrumsT è sofware libero che rispetta la tua libertà!
 Nome Nome del Database Importato Nessuna Assenza Note/Promemoria Nella finestra 'Lezioni precedenti' ci sono dati non salvati..
Chiudere ugualmente? Nella finestra 'Lezione di Oggi' ci sono dati non salvati..
Chiudere ugualmente? Guida in linea Apri un database scolastico Apri registro lezioni Altro-1 Altro-2 Altro-3 Telefono Prego confermare Lezioni Precedenti Lezioni precedenti con %s Promemoria/Blocco-Notes Ripristina lo stato precedente Rudimenti Anno Scolastico Selezione anno scolastico Anno scolastico: Imposta un nuovo anno scolastico Impostare un nuovo anno scolastico al database in uso: Imposta nuovo anno scolastico Mostra o nascondi la barra di stato Mostra o nascondi la barra degli strumenti Mostra/Nascondi barra di stato Mostra/Nascondi barra strumenti Qualche finestra risulta ancora aperta!
..Salva i tuoi dati prima di chiudere l'applicazione. Inizia Nuova Lezione Lo studente è assente Argomenti di Studio Stili Successo ! Archiviato con successo! Cognome L'insegnante è assente The configuration file is wrong! Please, reinstall the drumsT application La cartella principale per il ripristino dei database non esiste più!
Vuoi importarne una esistente o crearne una nuova? Questa colonna non può essere modificata. Questa è la prima volta che avvii DrumsT.

DrumsT usa un solo percorso per salvare/aprire i database

- Per creare un nuovo database, premi il pulsante 'Crea'.
- Se già esiste un database, premi il pulsante 'Importa'.
- Premi il pulsante 'Esci' per uscire semplicemente. Identità già esistente:

NOME/COGNOME:  {0} {1}
TELEFONO:  {2}
INDIRIZZO:  {3}
DATA DI NASCITA:  {4}
DATA ISCRIZIONE:  {5}
LIVELLO:  {6}

Desideri salvare comunque? Anno scolastico già precedentemente impostato. Per usare DrumsT, è necessario aprire un database o crearne uno nuovo se non esiste Lezione di oggi Lezione odierna con %s Digita il nome della scuola o un identificatore di posizione. Questo creerà un nuovo database con la 
scuola denominata: Aggiorna nuove voci sul database Aggiorna nuovo profilo Votazioni Votazioni e Promemoria Votazioni/Segnalazioni Benvenuto ! Dove vuoi salvare ? Dove si trova la cartella 'DrumsT_DB' ? Cambia i dati dello studente Cancella i dati dello studente Impostazione data lezione {0} {1} con ID class {2} è stato cancellato 