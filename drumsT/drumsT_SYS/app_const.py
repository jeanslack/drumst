#!/usr/bin/env python
# -*- coding: utf-8 -*-#
#
# Author:  Werner F. Bruhin
# Purpose: Application constants
# Created: 06/04/2012

import wx

# language domain
langDomain = "drumst"
# languages you want to support
supLang = {"it": wx.LANGUAGE_ITALIAN,
          }
