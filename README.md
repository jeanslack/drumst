# **DrumsT** is a School Management For Drum Teachers.   

* [View progect on PyPi](https://pypi.org/project/drumst/)
* [Gitlab Page](https://gitlab.com/jeanslack/drumst)

## Description

DrumsT is a school manager application open-source and
cross-platform, focused to individual lessons and designed
for drums teachers. It can handle independently multiple
school locations with progressive data storing of several
school years and students lessons who learns
the art of drumming.
DrumsT is free software that respects your freedom!

## Features

- Unlimited subscription of different schools in chronological order

- Storing students profile data: ID, Name, Surname, School year, Phone, 
Address, Birth date, Join Date, Course Level.

- The lesson tables are pre-formatted  with the following cells of study topics: 
Rudiments, Independency and Coordination, Chart reading, Hands/Foots settings, 
Styles and Rhythm languages, Music bases and others.

- Setting of attendances, lessons date and feature as votes, reminder and notes.

- Overall lessons list view with editable cells, undo and commit.

- DrumsT use a SQLite library already included on python packages ("batteries included")

- The system to index data in the tables is relational type.

## Requires

- Python >= 3  

- WxPython >= 4

## License and Copyright

Copyright © 2015 - 2019 Gianluca Pernigotto   
Author and Developer: Gianluca Pernigotto   
Mail: <jeanlucperni@gmail.com>   
License: GPL3 (see COPYING file in the source folder)
